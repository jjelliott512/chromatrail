﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameColor
{


    static GameColor Green { get { return new GameColor(Color.green); } }
    static GameColor Red { get { return new GameColor(Color.red); } }
    static GameColor Blue { get { return new GameColor(Color.blue); } }
    static GameColor Yellow { get { return new GameColor( Color.green + Color.red); } }
    static GameColor Cyan { get { return new GameColor(Color.green + Color.blue); } }
    static GameColor Purple { get { return new GameColor(Color.red + Color.blue); } }

    public enum Colors { Green, Red, Blue, Yellow, Cyan, Purple }
    static GameColor[] Values = { Green, Red, Blue, Yellow, Cyan, Purple };
    public Color color { get; }

    public GameColor(Color inColor)
    {
        inColor.a = 1;
        color = inColor;
        
    }

    public static bool operator ==(GameColor lhs, GameColor rhs)
    {
        return lhs.color == rhs.color;
    }
    public static bool operator !=(GameColor lhs, GameColor rhs)
    {
        return lhs.color == rhs.color;
    }

    public static GameColor operator +(GameColor lhs , GameColor rhs)
    {
        var newColor = new GameColor(lhs.color + rhs.color);
        return newColor;
    }
    public static GameColor operator -(GameColor lhs, GameColor rhs)
    {
        return new GameColor(lhs.color - rhs.color);
    }

    public static GameColor GetColor(Colors c)
    {
        return Values[(int)c];
    }

    public static Colors GetColorName(Color inColor)
    {
        for ( int i = 0; i < Values.Length; i++ )
        {
            if (Values[i].color == inColor) return (Colors)i;
        }
        return (Colors)0;
    }
}
