﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour {

    public GameColor.Colors MyColor;
    GameColor color;
    public ParticleSystem Burst;

	void Start () {
        color = GameColor.GetColor(MyColor);
        Burst.startColor = color.color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D( Collider2D col )
    {
        if (col.gameObject.CompareTag("Player") && col.gameObject.GetComponent<PlayerColor>().TotalColor == color)
        {
            Burst.Play();
            Destroy(gameObject);
        }
    }
}
