﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : MonoBehaviour {

    Stack<GameColor> MyColors;
    ColliderTrailRenderer Trail;
    SpriteRenderer Rend;
    public GameColor TotalColor;
    ParticleSystem ColorPopEffect;

	void Start () {

        Rend = GetComponent<SpriteRenderer>();
        Trail = GetComponent<ColliderTrailRenderer>();
        MyColors = new Stack<GameColor>();
        ColorPopEffect = GetComponent<ParticleSystem>();
        TotalColor = new GameColor(Color.black);
        UpdateColor();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            DropColor();
        }

	}

    public void AddColor(GameColor c)
    {
        if (MyColors.Contains(c) || TotalColor == c) return;
        MyColors.Push(c);
        UpdateColor();
    }

    public void DropColor() {

        if(MyColors.Count > 0)
        {
            MyColors.Pop();
            UpdateColor();
            ColorPopEffect.startColor = TotalColor.color;
            ColorPopEffect.Play();
        }
        
    }

    void UpdateColor()
    {
        GameColor color = new GameColor(Color.black);
        if(MyColors.Count == 0)
        {
            color = new GameColor(Color.black);
        } else
        {
            foreach(GameColor c in MyColors)
            {
                color += c;
            }
        }

        TotalColor = color;
        Rend.color = TotalColor.color;
        Trail.trail.GetComponent<Renderer>().material.color = TotalColor.color;
        ColorPopEffect.startColor = TotalColor.color;

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.CompareTag("ColorZone"))
        {
            AddColor(GameColor.GetColor(col.gameObject.GetComponent<ColorZone>().MyColor));
        }
    }
}
