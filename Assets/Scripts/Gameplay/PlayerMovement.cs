﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float MovementSpeed = 1;

    void Start()
    {
    }
	// Update is called once per frame
	void Update () {
        transform.Translate(MovementSpeed * ReadInput());
	}

    Vector2 ReadInput()
    {
        Vector2 axis = new Vector2();
        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");

        return axis.normalized;
    }
}
