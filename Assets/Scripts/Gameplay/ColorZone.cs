﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorZone : MonoBehaviour {

    public GameColor.Colors MyColor;
    public bool IsWandering;
    public float WanderSpeed, WanderRandomness, WanderInterval;
    private float lastTurnTime = -1;
    private Vector3 currentRotation;
    private Vector2 currentHeading;
	void Start () {

        GetComponent<SpriteRenderer>().color = GameColor.GetColor(MyColor).color;
	}
	
	// Update is called once per frame
	void Update () {
        if (IsWandering)
        {
            Wander();
        }
	}

    void Wander()
    {
        if(lastTurnTime == -1 || Time.time - lastTurnTime > WanderInterval)
        {
            currentRotation = new Vector3(0, 0, Random.Range(-WanderRandomness, WanderRandomness));
            currentHeading = Random.onUnitSphere;
            lastTurnTime = Time.time;
        }
        transform.Rotate(currentRotation);
        transform.Translate(currentHeading * WanderSpeed, Space.World);
    }
}
